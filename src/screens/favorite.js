import React, { Component } from 'react';
import { View, StyleSheet, Alert } from 'react-native';
import { remove, isArray } from 'lodash';
import Carousel from 'react-native-snap-carousel';
import { UserCard } from '../components/user-card';
import { Metrics } from '../common/metrics';
import { connect } from 'react-redux'
import { updateFavorite } from '../redux/actions/favorite-action';
import { showConfirm, showMessage } from '../utils/alert';

const ItemWidth = Metrics.WINDOW_WIDTH * 0.9;
const ItemHeight = ItemWidth * 1.1;
const PaddingCarousel = (Metrics.WINDOW_WIDTH - ItemWidth) / 2
class Favorite extends Component {
  state = {
    users: [],
    loading: true,
  };
  componentDidMount() {
    this.getUsers();
  }

  componentDidUpdate(prevProps) {
    if (prevProps !== this.props) {
      this.getUsers();
    }
  }

  getUsers = async () => {
    const { favorites } = this.props;
    if (isArray(favorites.data) && favorites.data.length > 0) {
      this.setState({
        users: favorites.data,
        loading: false
      }, () => this.forceUpdate());
    } else {
      this.setState({
        loading: false
      });
      showMessage('No any items');
    }
  };

  deleteUser = (user) => {
    const { updateFavorite } = this.props;
    showConfirm({
      message: 'Delete this item',
      onOk: () => {
        const users = this.state.users;
        remove(users, item => item.md5 === user.md5);
        this.setState(
          {
            users: users,
          },
          () => this.forceUpdate(),
        );
        updateFavorite(users);
      }
    })
  }

  render() {
    const { users } = this.state;
    return (
      <View style={styles.container}>
        <View style={styles.temp1} />
        <View style={styles.caroselContainer}>
          <View style={styles.temp2} />
          <Carousel
            style={styles.carosel}
            data={users}
            sliderWidth={Metrics.WINDOW_WIDTH}
            itemWidth={Metrics.WINDOW_WIDTH}
            inactiveSlideScale={1}
            activeSlideAlignment="center"
            containerCustomStyle={styles.containerCustomStyle}
            inactiveSlideOpacity={1}
            itemHeight={ItemHeight}
            hasParallaxImages={true}
            renderItem={({ item, index }) => (
              <UserCard
                width={ItemWidth}
                userInfo={item}
                deleteItem={userInfo => this.deleteUser(userInfo)}
              />
            )}
          />
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  carosel: {
    height: 200,
  },
  caroselContainer: {
    height: ItemHeight,
    zIndex: 9,
    backgroundColor: '#f9f9f9',
  },
  containerCustomStyle: {
    paddingHorizontal: PaddingCarousel,
  },
  temp1: {
    width: '100%',
    height: Metrics.WINDOW_HEIGHT * 0.2,
    backgroundColor: '#2c2e31',
  },
  temp2: {
    width: '100%',
    height: ItemWidth * 0.2,
    position: 'absolute',
    zIndex: 0,
    marginRight: -10,
    backgroundColor: '#2c2e31',
  },
});

const mapStateToProps = (state) => ({
  favorites: state.favorite
})

const mapDispatchToProps = dispatch => ({
  updateFavorite: data => dispatch(updateFavorite(data))
})

export default connect(mapStateToProps, mapDispatchToProps)(Favorite)
