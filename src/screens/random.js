import React, { Component } from 'react'
import {
  View,
  StyleSheet
} from 'react-native'
import { connect } from 'react-redux'
import { findIndex, isArray, isEmpty } from 'lodash'
import { UserCard } from '../components/user-card'
import { Metrics } from '../common/metrics'

import Swiper from 'react-native-deck-swiper'
import { updateFavorite } from '../redux/actions/favorite-action'
import { showMessage } from '../utils/alert'

const ItemWidth = Metrics.WINDOW_WIDTH * 0.9
const TopOfSwiper = Metrics.WINDOW_HEIGHT * 0.2
export class Random extends Component {
  state = {
    users: [],
    loading: true
  }
  componentDidMount() {
    this.getUsers()
  }

  getUsers = (addToTop = false) => {
    let { users } = this.state
    fetch('https://randomuser.me/api/0.4/?randomapi&&results=10')
      .then(res => res.json())
      .then(async userInfo => {
        if (userInfo) {
          let newData = userInfo.results.map(item => item.user)
          let tempData = users.concat(newData)
          if (addToTop) {
            tempData = newData.concat(users)
          }
          this.setState({
            users: tempData,
            loading: false
          })
        }
      })
      .catch(e => console.log(e))
  }

  onSwiped = (cardIndex) => {
    const { users } = this.state
    if (cardIndex === users.length - 4) {
      this.getUsers()
    }
  }
  addToFavorite = user => {
    const { favorites, updateFavorite } = this.props
    let data = this.props.favorites.data
    if (isArray(data) && !isEmpty(user)) {
      if (data.length > 0) {
        let index = findIndex(favorites, item => item.md5 === user.md5)
        if (index !== -1) return showMessage('Added to Favorited')
      }
      let newFavorites = data.concat([user])
      updateFavorite(newFavorites)
      showMessage('Added to Favorited')
    }

  }

  render() {
    const { users } = this.state
    return (
      <View style={styles.container}>
        <Swiper
          cards={users}
          renderCard={item => <UserCard width={ItemWidth} userInfo={item} />}
          onSwiped={this.onSwiped}
          onSwipedRight={index => this.addToFavorite(users[index])}
          verticalSwipe={false}
          infinite={false}
          backgroundColor={'#f9f9f9'}
          stackSize={3}
          cardVerticalMargin={0}
          marginTop={TopOfSwiper}
        >
          <View style={styles.temp1} />
        </Swiper>
      </View>
    )
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#2c2e31'
  },
  temp1: {
    width: '100%',
    height: ItemWidth * 0.2,
    position: 'absolute',
    zIndex: 0,
    top: 0,
    backgroundColor: '#2c2e31'
  }
})

const mapStateToProps = (state) => ({
  favorites: state.favorite
})

const mapDispatchToProps = dispatch => ({
  updateFavorite: data => dispatch(updateFavorite(data)),
})

export default connect(mapStateToProps, mapDispatchToProps)(Random)
