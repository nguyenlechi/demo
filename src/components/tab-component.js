import React from 'react'
import { TouchableOpacity, Text, StyleSheet } from 'react-native'
import PropTypes from 'prop-types';

export const TabComponent = ({ name, focused, onPress }) => {
  return (
    <TouchableOpacity
      activeOpacity={1}
      style={[styles.tabContainer, focused ? styles.activeTab : {}]}
      onPress={() => onPress()}
    >
      <Text style={[styles.title, focused ? styles.nameActive : {}]}>
        {name}
      </Text>
    </TouchableOpacity>
  )
}

const styles = StyleSheet.create({
  bottom: {
    position: 'absolute',
    bottom: 0,
    flexDirection: 'row',
    borderTopWidth: 1,
    borderTopColor: 'rgba(0,0,0,.25)'
  },
  tabContainer: {
    width: '50%',
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff'
  },
  activeTab: {
    backgroundColor: '#2c2e31'
  },
  name: {
    fontSize: 20,
    color: '#2c2e31'
  },
  nameActive: {
    color: '#fff'
  }
})

TabComponent.propTypes = {
  name: PropTypes.string,
  focused: PropTypes.bool,
  onPress: PropTypes.func
}
