import React, { Component } from 'react';
import {
  View,
  Image,
  StyleSheet,
  TouchableOpacity,
  ActivityIndicator,
  Text,
} from 'react-native';
import { isEmpty, isFunction } from 'lodash';
import PropTypes from 'prop-types';

const IconInfo = [
  {
    id: 1,
    iconActive: require('../assets/icon/user_active.png'),
    icon: require('../assets/icon/user.png'),
  },
  {
    id: 2,
    iconActive: require('../assets/icon/address_active.png'),
    icon: require('../assets/icon/address.png'),
  },
  {
    id: 3,
    iconActive: require('../assets/icon/call_active.png'),
    icon: require('../assets/icon/call.png'),
  },
  {
    id: 4,
    iconActive: require('../assets/icon/lock_active.png'),
    icon: require('../assets/icon/lock.png'),
  },
];

export class UserCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      activeIndex: 1
    };
  }
  setInfoActive(id) {
    this.setState({
      activeIndex: id
    });

  }

  createInfo(userInfo) {
    const { activeIndex } = this.state;
    switch (activeIndex) {
      case 1:
        return {
          lable: 'My name is',
          value: `${userInfo.name.first} ${userInfo.name.last}`
        }
      case 2:
        return {
          lable: 'My address is',
          value: `${userInfo.location.street} ${userInfo.name.city}`
        }
      case 3:
        return {
          lable: 'My phone number is',
          value: `${userInfo.phone}`
        }
      case 4:
        return {
          lable: 'My password is',
          value: `${userInfo.password}`
        }
      default:
        return {
          lable: 'My name is',
          value: `${userInfo.name.first} ${userInfo.name.last}`
        };
    }
  }

  renderInfoText = (userInfo) => {
    let info = this.createInfo(userInfo)
    return (
      <View style={styles.info}>
        <Text style={styles.lable}>{info.lable}</Text>
        <Text numberOfLines={1} style={styles.value}>
          {info.value}
        </Text>
      </View>
    )
  }

  render() {
    const { activeIndex } = this.state;
    const { userInfo, width, deleteItem } = this.props;
    const heightSlide = width;
    const heightHeader = width * 0.4 - 20;
    const widthAvatarContainer = width * 0.4;
    const avatarRadius = widthAvatarContainer / 2
    const avatarSource = { uri: userInfo && userInfo.picture ? userInfo.picture : 'http://api.randomuser.me/portraits/men/25.jpg' }

    if (isEmpty(userInfo)) {
      return (
        <View style={[styles.container, { width: width, height: heightSlide }]}>
          <ActivityIndicator size={'large'} />
        </View>
      )
    }
    return (
      <View style={[styles.container, { width: width, height: heightSlide }]}>
        <View style={[styles.header, { height: heightHeader }]}>
          {isFunction(deleteItem) && (
            <TouchableOpacity
              activeOpacity={1}
              style={styles.close}
              onPress={() => deleteItem(userInfo)}>
              <Text style={styles.closeText}>
                X
              </Text>
            </TouchableOpacity>
          )}

          <View
            style={[
              styles.avatarContainer,
              {
                height: widthAvatarContainer,
                width: widthAvatarContainer,
                borderRadius: avatarRadius,
              },
            ]}>
            <Image
              source={avatarSource}
              style={[
                styles.avatar,
                { borderRadius: avatarRadius },
              ]}
            />
          </View>
        </View>
        {this.renderInfoText(userInfo)}
        <View style={styles.iconList}>
          {IconInfo.map(item => {
            let imageSrc =
              item.id === activeIndex ? item.iconActive : item.icon;
            return (
              <TouchableOpacity
                activeOpacity={1}
                key={item.id}
                onPress={() => this.setInfoActive(item.id)}
                style={styles.typeInfoButton}>
                <Image source={imageSrc} style={styles.iconInfo} />
              </TouchableOpacity>
            );
          })}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    alignItems: 'center',
    backgroundColor: '#fff',
    display: 'flex',
    justifyContent: 'space-between',
    borderRadius: 10,
    shadowOffset: { width: 1, height: 1 },
    shadowColor: 'rgba(0,0,0,.25)',
    shadowOpacity: 0.4,
    elevation: 1
  },
  header: {
    width: '100%',
    height: '27%',
    borderBottomWidth: 1,
    backgroundColor: '#f9f9f9',
    borderColor: 'rgba(0,0,0,.25)',
    alignItems: 'center',
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
  },
  avatarContainer: {
    marginTop: 20,
    backgroundColor: '#fff',
    borderWidth: 1,
    borderColor: 'rgba(0,0,0,.25)',
    padding: 3,
    marginBottom: -70,
    zIndex: 99,
  },
  avatar: {
    height: '100%',
    width: '100%',
  },
  info: {
    paddingTop: 20,
  },
  lable: {
    textAlign: 'center',
    fontSize: 20,
    color: '#999',
  },
  value: {
    textAlign: 'center',
    fontSize: 30,
    color: '#2c2e31',
    paddingHorizontal: 5,
  },
  iconList: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginBottom: 15,
  },
  typeInfoButton: {
    marginHorizontal: 1,
  },
  iconInfo: {
    width: 41,
    height: 47,
  },
  close: {
    position: 'absolute',
    top: 0,
    right: 5,
    padding: 7
  },
  closeText: {
    fontSize: 18,
    color: 'rgba(0,0,0,.45)'
  }
});

UserCard.propTypes = {
  userInfo: PropTypes.object,
  width: PropTypes.number,
  deleteItem: PropTypes.func
}
