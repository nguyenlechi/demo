import { TYPES } from "../actions/action-type";

const INIT_STATE = {
    data: []
}

export const favorite = (state = INIT_STATE, action) => {
    switch (action.type) {
        case TYPES.UPDATE_FAVORITE:
            return {
                data: action.data
            };
        case TYPES.DELETE_FAVORITE:
            return INIT_STATE
        default: return state;
    }
}