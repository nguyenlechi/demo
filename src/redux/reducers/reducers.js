import { combineReducers } from 'redux'
import { favorite } from './favorite'

export const reducers = (state, action) => combineReducers({ favorite })(state, action)