import { TYPES } from "./action-type"

export const updateFavorite = (data) => {
  return {
    type: TYPES.UPDATE_FAVORITE,
    data: data
  }
}
export const deleteFavorite = () => {
  return {
    type: TYPES.DELETE_FAVORITE,
  }
}