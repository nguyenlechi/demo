import { Alert } from 'react-native'
export const showConfirm = ({ message, onOk }) =>
  Alert.alert(
    'Alert',
    message,
    [
      {
        text: 'Cancel',
        style: 'cancel',
      },
      {
        text: 'OK',
        onPress: () => onOk && onOk(),
        style: 'cancel'
      }
    ],
    { cancelable: true }
  )

export const showMessage = (message: String) =>
  Alert.alert(
    'Alert',
    message,
    [{ text: 'OK', style: 'cancel' }],
    { cancelable: false }
  )

