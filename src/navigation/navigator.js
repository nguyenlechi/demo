import React from 'react'
import { createAppContainer } from 'react-navigation'
import { createBottomTabNavigator } from 'react-navigation-tabs'
import Random from '../screens/random'
import Favorite from '../screens/favorite'
import { TabComponent } from '../components/tab-component'

const TabNavigator = createBottomTabNavigator({
  Random: {
    screen: Random,
    navigationOptions: {
      tabBarButtonComponent: ({ focused, onPress }) => <TabComponent onPress={onPress} name='Random' focused={focused} />
    }
  },
  Favorite: {
    screen: Favorite,
    navigationOptions: {
      tabBarButtonComponent: ({ focused, onPress }) => <TabComponent onPress={onPress} name='Favorite' focused={focused} />
    }
  }

}, {
  animationEnabled: true,
  swipeEnabled: true,
  tabBarPosition: 'bottom',
  initialRouteName: 'Random',
})

export default createAppContainer(TabNavigator);