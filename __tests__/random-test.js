import React from 'react';
import "isomorphic-fetch"
import renderer from 'react-test-renderer';
import { Random } from '../src/screens/random';
import { showMessage } from '../src/utils/alert';
describe('Testing connected react-redux component.', () => {

  const userInfo = {
    name: {
      first: 'Le',
      last: 'Nguyen'
    },
    location: {
      street: 'Au Co',
      city: 'Ha Noi'
    },
    phone: '0987675412',
    password: 'passExam',
    md5: 'abc123'
  };

  it('should detect duplicated targets', () => {

    const props = {
      favorites: {
        data: [userInfo]
      },
      updateFavorite: jest.fn().mockReturnValue({})
    }
    const test = renderer.create(<Random {...props} />).getInstance()

    expect(test.addToFavorite(userInfo)).toEqual(showMessage('Added to Favorite'))
  });


  it('should dispatch updateFavorite action ', () => {
    const props = {
      favorites: {
        data: []
      },
      updateFavorite: jest.fn().mockReturnValue({})
    }
    const test = renderer.create(<Random {...props} />).getInstance()

    test.addToFavorite(userInfo)

    expect(test.props.updateFavorite.mock.calls.length).toBe(1);
  });

});