import 'react-native'
import * as favoriteReducer from '../src/redux/reducers/favorite'
import {TYPES} from '../src/redux/actions/action-type'

const favoriteTestData = {
  data:[{
    name:'favorite'
  }]
}

describe('reducers', ()=>{
  it('should return the init state',()=>{
    let initState = {data:[]}
    expect(favoriteReducer.favorite(undefined, {})).toEqual(initState)
  })

  it('should return favoriteTestData ', ()=>{
    expect(favoriteReducer.favorite(undefined, {
      type: TYPES.UPDATE_FAVORITE,
      data: favoriteTestData.data
    })).toEqual(favoriteTestData)
  })
})