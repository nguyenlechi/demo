import 'react-native';
import React from 'react'
import renderer from 'react-test-renderer'
import { UserCard } from '../src/components/user-card';

test('UserCard snapShot', ()=>{
  const userInfo={
    name:{
      first: 'Le',
      last: 'Nguyen'
    },
    location:{
      street: 'Au Co',
      city: 'Ha Noi'
    },
    phone:'0987675412',
    password: 'passExam'
  }
  const width = 200
  const snap = renderer.create(<UserCard userInfo={userInfo} width={width}/>).toJSON();
  expect(snap).toMatchSnapshot();
})