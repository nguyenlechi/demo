import 'react-native'
import { updateFavorite } from '../src/redux/actions/favorite-action'
import { TYPES } from '../src/redux/actions/action-type'

describe('actions', () => {
  it('should create an action to add a todo', () => {
    const data = 'Test redux'
    const expectedAction = {
      type: TYPES.UPDATE_FAVORITE,
      data
    }
    expect(updateFavorite(data)).toEqual(expectedAction)
  });
})
